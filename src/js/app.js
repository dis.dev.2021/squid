import './plugins/import-jquery';
import 'focus-visible';
import accordion from './components/accordion';
import lazyImages from './plugins/lazyImages';
import documentReady from './helpers/documentReady';
import mobileNav from './components/mobile-nav';
import maskPhone from './plugins/phone-mask';
import { Fancybox } from "@fancyapps/ui";
import {WebpMachine} from "webp-hero";
import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper/swiper-bundle';
Swiper.use([Navigation, Pagination, Autoplay, Thumbs]);


documentReady(() => {
    lazyImages();
    mobileNav();
    accordion();
    maskPhone('input[name="phone"]');

    // IE Webp Support
    const webpMachine = new WebpMachine();
    webpMachine.polyfillDocument();


    const changeHeight = () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    };
    changeHeight();
 //   window.addEventListener('resize', () => {
 //       changeHeight();
 //   });

    // Modal
    Fancybox.bind('[data-fancybox]', {
        autoFocus: false
    });

    const countdown = document.querySelector('.countdown');
    if (countdown) {
        function getTimeRemaining(endtime) {
            let t = Date.parse(endtime) - Date.parse(new Date());
            let seconds = Math.floor((t / 1000) % 60);
            let minutes = Math.floor((t / 1000 / 60) % 60);
            let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            let days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {
            let clock = document.getElementById(id);
            let daysSpan = clock.querySelector('.days');
            let hoursSpan = clock.querySelector('.hours');
            let minutesSpan = clock.querySelector('.minutes');
            let secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
                let t = getTimeRemaining(endtime);

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0) {
                    clearInterval(timeinterval);
                }
            }

            updateClock();
            let timeinterval = setInterval(updateClock, 1000);
        }

        let deadline = new Date(Date.parse(new Date()) + 3 * 24 * 60 * 60 * 1000); // for endless timer
        initializeClock('countdown', deadline);
    }

    // Video
    let videoEl = document.getElementsByClassName('video-player') [0];
    let playBtn = document.getElementsByClassName('btn-play') [0];
    if (videoEl) {
        playBtn.addEventListener('click', function () {
            if (videoEl.paused) {
                videoEl.play();
            } else {
                videoEl.pause();
            }
        }, false);

        videoEl.addEventListener('play', function () {

            playBtn.classList.add("pause");
        }, false);

        videoEl.addEventListener('pause', function () {

            playBtn.classList.remove("pause");
        }, false);
    }


    // Auth avatars
    let authAvatar = new Swiper(".auth__avatar--slider", {
        slidesPerView: 3,
        spaceBetween: 16,
        navigation: {
            nextEl: '.auth__nav--next',
            prevEl: '.auth__nav--prev',
        },
        breakpoints: {
            576: {
                slidesPerView: 4,
                spaceBetween: 12,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 8,
            },
            992: {
                slidesPerView: 4,
                spaceBetween: 8
            },
        },
    });
});


