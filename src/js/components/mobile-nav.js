export default () => {
    const pageNavToggle = document.querySelectorAll('.nav-toggle');

    if (pageNavToggle) {
        pageNavToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                const self = document.querySelector('html');
                self.classList.toggle('nav-open');
                return false;
            });
        });
    }
};
