import gulp from "gulp";
import config from "../config";

const videoSource = () => (
    gulp.src(`${config.src.video}/**/*`)
        .pipe(gulp.dest(config.dest.video))
);

export const videoBuild = gulp.parallel(videoSource);

export const videoWatch = () => {
    gulp.watch(`${config.src.video}/**/*`, videoSource);
};



